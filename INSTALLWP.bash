#!/usr/bin/env bash

###
###
###
### Install Wordpress on a fresh AWS instance of Ubuntu 16.04
###
###
###

# Update and add all the things
sudo apt update
sudo apt upgrade -y
sudo apt install -y unzip htop whowatch traceroute mysql-server nginx mysql-server mysql-client php-curl php-gd php-mbstring php-mcrypt php-xml php-xmlrpc php-fpm php-mysql virtualbox-guest-dkms
sudo apt autoremove -y
sudo apt autoclean -y

# Some simple preemptive security measures and optimizations
#mysql_secure_installation
sudo sed -ri 's/\;cgi.fix_pathinfo=1/cgi.fix_pathinfo=1/' /etc/php/7.0/fpm/php.ini
sudo adduser ${USER} www-data
sudo adduser ${USER} plugdev
sudo adduser ${USER} netdev
sed -ri 's/#force_color_prompt=yes/force_color_prompt=yes/' ~/.bashrc

### Now is the time to do any hostname changing, before we get too far
#sudo nano /etc/hosts
#sudo nano /etc/hostname

# Configure MySQL
PASSWDDB=$(openssl rand -base64 32)
MAINDB="wordpress_site"
echo "Please enter root user MySQL password!"
read rootpasswd
mysql -uroot -p${rootpasswd} -e "CREATE DATABASE ${MAINDB} /*\!40100 DEFAULT CHARACTER SET utf8 */;"
mysql -uroot -p${rootpasswd} -e "CREATE USER ${MAINDB}@localhost IDENTIFIED BY '${PASSWDDB}';"
mysql -uroot -p${rootpasswd} -e "GRANT ALL PRIVILEGES ON ${MAINDB}.* TO '${MAINDB}'@'localhost';"
mysql -uroot -p${rootpasswd} -e "FLUSH PRIVILEGES;"
echo "Using password ''$PASSWDDB'' for user ''$MAINDB@localhost''."

# Install and configure WP
sudo chown -R www-data:www-data /usr/share/nginx
sudo chmod -R ug+rw /usr/share/nginx
sudo find /usr/share/nginx -type d -exec chmod g+s {} \;
cd /usr/share/nginx
sudo curl -O https://wordpress.org/latest.tar.gz
sudo tar xvzf latest.tar.gz 
sudo chown -R www-data:www-data wordpress
sudo chmod -R ug+rw wordpress
cd wordpress
sudo cp wp-config-sample.php wp-config.php
sudo sed -ri 's/database_name_here/wordpress_site/' wp-config.php
sudo sed -ri 's/username_here/wordpress_site/' wp-config.php
sudo sed -ri "s%password_here%${PASSWDDB}%g" wp-config.php

# Config the webserver
sudo wget -O /etc/nginx/sites-available/wordpress_site https://bitbucket.org/yeticraft/bitsandbobs/raw/master/wordpress_site.nginx.conf
sudo ln -s /etc/nginx/sites-available/wordpress_site /etc/nginx/sites-enabled/
sudo rm -f /etc/nginx/sites-enabled/default

truncate --size 0 ~/.bash_history && history -c
sudo reboot

