#Usage

Quick and simple staging script for use on AWS based Ubuntu 16.-4 instances.  This will install and update the system to run the latest Wordpress server components.

Log into your new virginal instance and execute the following command:

```
bash <(wget -O - https://bitbucket.org/yeticraft/bitsandbobs/raw/master/INSTALLWP.bash)
```

That's it.  So simple.

Do NOT use on an existing installation.  It will not work, and it will break things.  I may fix that at some point and make it more usable in a generic fashion.